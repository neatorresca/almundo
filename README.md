# ALMUNDO

## Download the repository:
$ git clone https://neatorresca@bitbucket.org/neatorresca/almundo.git

## Install dependencies
$ cd almundo/almundo; npm install; cd ../api; npm install

## Run api server:
$ cd ./almundo/api; npm start

## Run front end server:
$ cd ./almundo/almundo; ng serve. Then go to localhost:4200

## Set endpoints in API
Go to $ cd ./almundo/api/lib/config/ and configure the endpoints you wish for database and cache! They will be printed in console once you raise the server using that environment!

## Use environments in API
You can use prod environment by executing $ NODE_ENV=prod node app.js, staging and development are also available.

## Set endpoints in front end
Go to $ cd ./almundo/almundo/src/environments and set backend endpoints in each environment. You can also create environment.staging.ts or another you need!

## Use environments in front end
$ ng build --prod, or another environment you need to build.

## Watch in your mobile device!
Raise server with $ ng serve --host 0.0.0.0, then navigate in your mobile to your_private_ip:4200, don't forget to replace backend endpoint at ./almundo/almundo/src/environments/environment.ts from localhost into your_private_ip to appropiately communicate with backend server.

## Features
####1- You can search by hotel name, not case sensitive.
####2- You can filter hotels by stars.
####3- You can combine both filters.
####4- You can collapse each facet.
####5- In mobile, you can collapse the whole facet panel.
####6- A list of applied filters will be displayed.
####7- You can remove an applied filter.
####8- You can zoom by clicking hotel image or blue button.
####9- If an image did not load successfully, a default image will be displayed.
####10- Wireframe provided looks the same for resolutions 375px and 1366px. For others it is responsive.
####11- You can get hotel info by requesting url http://localhost:3000/hotels/id/#####, for example http://localhost:3000/hotels/id/249942